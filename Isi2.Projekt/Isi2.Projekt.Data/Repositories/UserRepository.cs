﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Isi2.Projekt.Contracts.Common;
using Isi2.Projekt.Contracts;
using Isi2.Projekt.Contracts.User;
using System.Web.Security;
using Isi2.Projekt.Contracts.Commons;

namespace Isi2.Projekt.Data
{
    public class UserRepository
    {
        private Entities entities = new Entities();

        public SaveContract Register(UserContract user)
        {
            SaveContract result = new SaveContract();

            var existing = this.GetUsers(new UserFilterContract { Login = user.Login }, null);
            if (existing.Items.Count > 0)
            {
                result.Errors = "Ta nazwa użytkownika jest już zajęta!";
                return result;
            }

            existing = this.GetUsers(new UserFilterContract { Email = user.Email }, null);
            if (existing.Items.Count > 0)
            {
                result.Errors = "Podany adres email jest już zarejestrowany w bazie!";
                return result;
            }
            
            entities.usrx_users.Add(new usrx_users
            {
                login = user.Login,
                email = user.Email,
                city = user.City,
                banned = user.Banned,
                password = user.Password,
                password_reminder = user.Password_reminder,
                date_last_login = user.Date_last_login,
                date_created = user.Date_created,
                ban_reason = user.Ban_reason,
                is_mod = user.Is_mod,
                is_admin = user.Is_admin
            });

            entities.SaveChanges();

            result.Success = true;

            return result;
        }

        public SaveContract ChangePassword(string login, string oldPassword, string newPassword)
        {
            var query = from u in entities.usrx_users where u.login == login select u;
            foreach (usrx_users usr in query)
                usr.password = newPassword;
            
            entities.SaveChanges();

            return new SaveContract { Success = true };
        }

        public PagedListResultContract<UserContract> GetUsers(UserFilterContract filter, PagedListRequestContract paging)
        {
            var query = entities.usrx_users.AsQueryable();

            if (filter != null)
            {
                if (filter.Id.HasValue)
                    query = query.Where(c => c.usrx_id == filter.Id);
                if (!String.IsNullOrEmpty(filter.Login))
                    query = query.Where(c => c.login == filter.Login);
                if (!String.IsNullOrEmpty(filter.Password))
                    query = query.Where(c => c.password == filter.Password);
                if (!String.IsNullOrEmpty(filter.Email))
                    query = query.Where(c => c.email == filter.Email);
            }

            var count = query.Count();

            if (paging != null)
            {
                query = query.OrderBy(c => c.usrx_id).Skip(paging.Page * paging.PageSize).Take(paging.Take);
            }

            var result = query.Select(c => new UserContract
            {
                Id = c.usrx_id,
                Login = c.login
            }).ToList();

            return new PagedListResultContract<UserContract> { Count = count, Items = result };
        }
    }
}

﻿using Castle.Facilities.WcfIntegration;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Isi2.Projekt.Contracts;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Isi2.Projekt.Web
{
    // Uwaga: aby uzyskać instrukcje włączania trybu klasycznego usług IIS6 lub IIS7, 
    // odwiedź stronę http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        public static WindsorContainer Container { get; set; }

        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                "Default", // Nazwa trasy
                "{controller}/{action}/{id}", // Adres URL z parametrami
                new { controller = "Entry", action = "Index", id = UrlParameter.Optional } // Ustawienia domyślne parametrów
            );

        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);
        }

        private void BuildContainer()
        {
            Container = new WindsorContainer();
            Container.Kernel.AddFacility<WcfFacility>();
            Container.Kernel.Register(Component.For<IService>()
                                               .ImplementedBy<Service.Service>()
                                               .Named("Service"));
        }
    }
}
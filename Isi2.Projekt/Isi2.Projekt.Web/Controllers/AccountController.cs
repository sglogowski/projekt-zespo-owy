﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using Isi2.Projekt.Web.Models;
using Castle.Windsor;
using Isi2.Projekt.Web.Helpers;
using Isi2.Projekt.Contracts;
using Isi2.Projekt.Contracts.User;
using Isi2.Projekt.Contracts.Common;
using System.Security.Cryptography;
using System.Text;

namespace Isi2.Projekt.Web.Controllers
{
    public class AccountController : Controller
    {
        private IService service = WindsorContainerAccess.GetContainer().Resolve<IService>();

        public ActionResult LogOn()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LogOn(LogOnModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                var password = GetSHA512Crypt(model.Password);
                var user = service.GetUsers(new UserFilterContract { Login = model.UserName, Password = password }, null);

                if (user.Items.Count == 1)
                {
                    FormsAuthentication.SetAuthCookie(model.UserName, model.RememberMe);
                    if (Url.IsLocalUrl(returnUrl) && returnUrl.Length > 1 && returnUrl.StartsWith("/")
                        && !returnUrl.StartsWith("//") && !returnUrl.StartsWith("/\\"))
                    {
                        return Redirect(returnUrl);
                    }
                    else
                    {
                        return RedirectToAction("Index", "Entry");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Podana nazwa użytkownika lub hasło są niepoprawne.");
                }
            }

            return View(model);
        }

        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();

            return RedirectToAction("Index", "Entry");
        }

        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Register(RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                var result = service.Register(new UserContract()
                {
                    Login = model.UserName,
                    Email = model.Email,
                    Password = GetSHA512Crypt(model.Password),
                    Date_last_login = DateTime.Now,
                    Date_created = DateTime.Now
                });

                if (result.Success)
                {
                    FormsAuthentication.SetAuthCookie(model.UserName, false);
                    return RedirectToAction("Index", "Entry");
                }
                ModelState.AddModelError("", result.Errors);
            }

            return View(model);
        }

        [Authorize]
        public ActionResult ChangePassword()
        {
            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordModel model)
        {
            if (ModelState.IsValid)
            {
                bool changePasswordSucceeded;
                try
                {
                    MembershipUser currentUser = Membership.GetUser(User.Identity.Name, true /* userIsOnline */);
                    changePasswordSucceeded = currentUser.ChangePassword(model.OldPassword, model.NewPassword);
                }
                catch (Exception)
                {
                    changePasswordSucceeded = false;
                }

                if (changePasswordSucceeded)
                {
                    return RedirectToAction("ChangePasswordSuccess");
                }
                else
                {
                    try
                    {
                        service.ChangePassword(User.Identity.Name, model.OldPassword, GetSHA512Crypt(model.NewPassword));
                        return RedirectToAction("ChangePasswordSuccess");
                    }
                    catch (Exception)
                    {
                        ModelState.AddModelError("", "Bieżące hasło jest niepoprawne lub nowe hasło jest nieprawidłowe.");
                    }
                }
            }

            return View(model);
        }

        public ActionResult ChangePasswordSuccess()
        {
            return View();
        }

        #region Status Codes
        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // Na stronie http://go.microsoft.com/fwlink/?LinkID=177550 znajduje się
            // pełna lista kodów stanu.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "Nazwa użytkownika już istnieje. Wprowadź inną nazwę użytkownika.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "Nazwa użytkownika dla tego adresu e-mail już istnieje. Wprowadź inny adres e-mail.";

                case MembershipCreateStatus.InvalidPassword:
                    return "Podane hasło jest nieprawidłowe. Wprowadź prawidłową wartość hasła.";

                case MembershipCreateStatus.InvalidEmail:
                    return "Podany adres e-mail jest nieprawidłowy. Sprawdź wartość i spróbuj ponownie.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "Podana odpowiedź dla funkcji odzyskiwania hasła jest nieprawidłowa. Sprawdź wartość i spróbuj ponownie.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "Podane pytanie dla funkcji odzyskiwania hasła jest nieprawidłowe. Sprawdź wartość i spróbuj ponownie.";

                case MembershipCreateStatus.InvalidUserName:
                    return "Podana nazwa użytkownika jest nieprawidłowa. Sprawdź wartość i spróbuj ponownie.";

                case MembershipCreateStatus.ProviderError:
                    return "Dostawca uwierzytelniania zwrócił błąd. Sprawdź wpis i spróbuj ponownie. Jeśli problem nie zniknie, skontaktuj się z administratorem systemu.";

                case MembershipCreateStatus.UserRejected:
                    return "Żądanie utworzenia użytkownika zostało anulowane. Sprawdź wpis i spróbuj ponownie. Jeśli problem nie zniknie, skontaktuj się z administratorem systemu.";

                default:
                    return "Wystąpił nieznany błąd. Sprawdź wpis i spróbuj ponownie. Jeśli problem nie zniknie, skontaktuj się z administratorem systemu.";
            }
        }
        #endregion

        private static string GetSHA512Crypt(string text)
        {
            string hash = "";
            SHA512 alg = SHA512.Create();
            byte[] result = alg.ComputeHash(Encoding.UTF8.GetBytes(text));
            hash = Encoding.UTF8.GetString(result);
            return hash;
        }
    }
}

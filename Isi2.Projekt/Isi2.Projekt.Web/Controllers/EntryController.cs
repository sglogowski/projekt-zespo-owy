﻿using Isi2.Projekt.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Isi2.Projekt.Web.Controllers
{
    public class EntryController : Controller
    {
        public ActionResult Index()
        {
            var items = new List<EntryContract>();

            for (int i = 0; i < 20; i++)
            {
                items.Add(new EntryContract { Id = 1, Title = "tytuł" + i, AuthorId = i, Created = DateTime.Now });
            }

            return View(items);
        }
    }
}

﻿using Isi2.Projekt.Contracts;
using Isi2.Projekt.Contracts.Common;
using Isi2.Projekt.Contracts.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Web;
using System.Web.Security;
using Isi2.Projekt.Contracts.Commons;

namespace Isi2.Projekt.Contracts
{
    [ServiceContract]
    public interface IService
    {
        [OperationContract]
        PagedListResultContract<UserContract> GetUsers(UserFilterContract filter, PagedListRequestContract paging);

        [OperationContract]
        SaveContract Register(UserContract user);

        [OperationContract]
        SaveContract ChangePassword(string login, string oldPassword, string newPassword);
    }
}

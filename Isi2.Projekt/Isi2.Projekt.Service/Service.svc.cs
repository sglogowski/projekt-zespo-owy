﻿using Isi2.Projekt.Contracts;
using Isi2.Projekt.Contracts.Common;
using Isi2.Projekt.Contracts.User;
using Isi2.Projekt.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Web.Security;
using Isi2.Projekt.Contracts.Commons;

namespace Isi2.Projekt.Service
{
    public class Service : IService
    {
        private UserRepository userRepository = new UserRepository();
        private EntryRepository entryRepository = new EntryRepository();

        public SaveContract Register(UserContract user)
        {
            return userRepository.Register(user);
        }

        public PagedListResultContract<UserContract> GetUsers(UserFilterContract filter, PagedListRequestContract paging)
        {
            return userRepository.GetUsers(filter, paging);
        }

        public SaveContract ChangePassword(string login, string oldPassword, string newPassword)
        {
            return userRepository.ChangePassword(login, oldPassword, newPassword);
        }
    }
}
